const got = require("got");

module.exports = class Rucaptcha {
  constructor({ apiKey, siteKey, pageUrl, pollInterval }) {
    this.apiKey = apiKey;
    this.siteKey = siteKey;
    this.pageUrl = pageUrl;
    this.pollInterval = pollInterval;
  }

  async solve(action, minScore) {
    const in_ = await got
      .post("https://rucaptcha.com/in.php", {
        form: {
          key: this.apiKey,
          method: "userrecaptcha",
          version: "v3",
          min_score: minScore,
          googlekey: this.siteKey,
          pageurl: this.pageUrl,
          action,
          json: "1",
        },
      })
      .json();
    const captchaId = in_.request;
    const tokenPromise = new Promise((resolve) => {
      const interval = setInterval(async () => {
        const res = await got("https://rucaptcha.com/res.php", {
          searchParams: {
            key: this.apiKey,
            action: "get",
            id: captchaId,
            json: "1",
          },
        }).json();
        console.log(res);
        if (res.status === 1) {
          resolve(res.request);
          clearInterval(interval);
        }
      }, this.pollInterval);
    });

    return {
      token: await tokenPromise,
      id: captchaId,
    };
  }

  async _report(captcha, good) {
    console.log(`reporting captcha ${captcha.id} as ${good ? "good" : "bad"}`);
    await got("https://rucaptcha.com/res.php", {
      searchParams: {
        key: this.apiKey,
        action: good ? "reportgood" : "reportbad",
        id: captcha.id,
      },
    });
  }

  async good(captcha) {
    await this._report(captcha, true);
  }

  async bad(captcha) {
    await this._report(captcha, false);
  }
};
