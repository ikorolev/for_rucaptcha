const got = require("got");
const UserAgent = require("user-agents");
const Rucaptcha = require("./rucaptcha");

const captchaSettings = {
  apiKey: "", // здесь ключ API от рукапчи
  siteKey: "6LdKJhMaAAAAAIfeHC6FZc-UVfzDQpiOjaJUWoxr",
  pageUrl: "https://www.reestr-zalogov.ru/search/index",
  pollInterval: 1000, // для теста, в продакшен 5000
};

const rucaptcha = new Rucaptcha(captchaSettings);

async function main() {
  const captcha = await rucaptcha.solve("search_notary", 0.9);

  const requestBody = {
    mode: "onlyActual",
    limit: 10,
    offset: 0,
    filter: {
      pledgor: {
        privatePerson: {
          firstName: "Иван",
          lastName: "Иванов",
          middleName: "",
          passport: "",
          district: "",
          birthday: "",
        },
      },
    },
  };

  const notaryResponse = await got.post("https://www.reestr-zalogov.ru/api/search/notary", {
    headers: {
      "user-agent": new UserAgent({
        deviceCategory: "desktop",
        platform: "Win32",
      }).toString(),
    },
    json: requestBody,
    searchParams: {
      token: captcha.token,
    },
    throwHttpErrors: false,
  });

  if (notaryResponse.statusCode === 200) {
    await rucaptcha.good(captcha);
  } else if (notaryResponse.statusCode === 403) {
    await rucaptcha.bad(captcha);
  }

  console.log(notaryResponse.statusCode);
  console.log(notaryResponse.body);
}

main().catch((err) => console.error(err));
